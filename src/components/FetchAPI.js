import { Component } from "react";

class FetchAPI extends Component {

    fetchAPI = async (url, body) => {
        let response = await fetch(url, body);
        let data = await response.json();
        return data;
    }
    getAllAPI = () => {
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts")
            .then((data) => {
                console.log(data);
            })
    }

    getByIDAPI = () => {
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts/1")
            .then((data) => {
                console.log(data);
            })
    }

    postAPI = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };

        this.fetchAPI("https://jsonplaceholder.typicode.com/posts", body)
            .then((data) => {
                console.log(data);
            })
    }

    updateByIDAPI = () => {
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                id: 1,
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        this.fetchAPI("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
    }

    deleteByAPI = () => {
        let body = {
            method: 'DELETE',
        };

        this.fetchAPI("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
    }

    render() {
        return (
            <div className="row mt-2">
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getAllAPI}>Get all API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getByIDAPI}>Get by Id API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.postAPI}>Post API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.updateByIDAPI}>Update by Id API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.deleteByAPI}>Delete by Id API</button>
                </div>
            </div>
        )
    }
}
export default FetchAPI;